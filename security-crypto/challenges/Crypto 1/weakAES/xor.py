def sxors(s1,s2):
    return [ ord(a) ^ ord(b) for (a,b) in zip(s1, s2) ]

def ixori(i1, i2):
    return [ a ^ b for (a,b) in zip(i1, i2) ]

def sxori(s1, i2):
    return [ ord(a) ^ b for (a,b) in zip(s1, i2) ]




val1 = "vite"
val2 = "casa"
null_val =  [0, 0, 0, 0]


x1 = sxors(val1, val2)
print(f"{val1} xor {val2} = {x1}")


x1 = sxors(val1, val1)
print(f"{val1} xor {val1} = {x1}")


x2 = sxori(val1, null_val)
res = ''.join(chr(x) for x in x2)
print(f"{val1} xor {null_val} = {res}")
